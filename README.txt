Commerce Discount Payment Method
===============

Description
-----------

Module implements discount inline condition to apply discount depending
on payment method selected during order checkout process.

It overrides payment checkout pane to properly react on payment method selection
calling commerce_cart_order_refresh() method.

You may want to use hook
hook_commerce_discount_payment_method_pane_ajax_callback_commands_alter($commands, $form_state)
to alter the way your checkout page elements are updated when payment method
selection changes.
