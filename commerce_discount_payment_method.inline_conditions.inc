<?php

/**
 * @file
 * Provides Inline Conditions integration for the Commerce Discount Saleprice module.
 */

/**
 * Implements hook_inline_conditions_info().
 */
function commerce_discount_payment_method_inline_conditions_info() {
  $conditions = array();

  $conditions['commerce_discount_order_has_payment_method'] = array(
    'label' => t('Payment method'),
    'entity type' => 'commerce_order',
    'callbacks' => array(
      'configure' => 'commerce_discount_payment_method_order_has_payment_method_configure',
      'build' => 'commerce_discount_payment_method_order_has_payment_method_build',
    ),
  );

  return $conditions;
}

/**
 * Configuration callback for commerce_discount_order_has_payment_method.
 *
 * @param array $settings
 *   Values for the form element.
 *
 * @return array
 *   Return a form element.
 */
function commerce_discount_payment_method_order_has_payment_method_configure($settings) {
  $form = array();

  $form['payment_method'] = array(
    '#type' => 'select',
    '#title' => t('Payment methods'),
    '#required' => TRUE,
    '#default_value' => isset($settings['payment_method']) ? $settings['payment_method'] : NULL,
    '#options' => array_column(commerce_payment_methods(), 'title', 'method_id'),
  );
  
  return $form;
}