<?php

/**
 * @file
 * commerce_discount_payment_method.rules.inc
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_discount_payment_method_rules_condition_info() {
  $inline_conditions = inline_conditions_get_info();
  
  $conditions = array();

  $conditions['commerce_discount_order_has_payment_method'] = array(
    'label' => t('Commerce order discount for payment method'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('Commerce order.'),
      ),
      'payment_method' => array(
        'type' => 'text',
        'label' => t('Payment method'),
        'description' => t('Payment method'),
      ),
    ),
    'module' => 'commerce_discount_payment_method',
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => $inline_conditions['commerce_discount_order_has_payment_method']['callbacks']['build'],
    ),
  );

  return $conditions;
}


/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_payment_method_rules_action_info() {
  $actions = array();

  // Add an action for each available payment method.
  $actions['commerce_discount_payment_method_disabling_reaction'] = array(
    'label' => t('Reacting on payment methods disabling'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order', array(), array('context' => 'a drupal commerce order')),
        'skip save' => TRUE,
      ),
    ),
    'group' => t('Commerce Payment'),
    'callbacks' => array(
      'execute' => 'commerce_discount_payment_method_disabling_reaction',
    ),
  );

  return $actions;
}

/**
 * Build callback for commerce_discount_order_has_payment_method on order.
 *
 * @param Object $order
 *   Entity type given by the rule.
 * @param string $payment_method
 *   Values for the condition settings.
 *
 * @return bool
 *   True if condition is valid, false otherwise.
 */
function commerce_discount_payment_method_order_has_payment_method_build($order, $payment_method) {
  return isset($order->data['payment_method']) && strpos($order->data['payment_method'], $payment_method) === 0;
}

/**
 * Rule action callback for commerce_discount_payment_method_disabling_reaction on order.
 *
 * @param Object $order
 *   Entity type given by the rule.
 */
function commerce_discount_payment_method_disabling_reaction($order) {
  $is_payment_method_discount_found = FALSE;
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  if (isset($order->commerce_discounts) && $order_wrapper->commerce_discounts->value()) {
    foreach ($order_wrapper->commerce_discounts as $discount_wrapper) {
      if ($discount_wrapper->inline_conditions->count()) {
        foreach ($discount_wrapper->inline_conditions as $condition_wrapper) {
          $condition = $condition_wrapper->value();
          if ($condition['condition_name'] == 'commerce_discount_order_has_payment_method') {
            $is_payment_method_discount_found = TRUE;
            break;
          }
        }
      }
    }

    if ($is_payment_method_discount_found && isset($order->data['payment_method'])) {
      $is_selected_payment_method_enabled = in_array($order->data['payment_method'], array_keys($order->payment_methods));
      if (!$is_selected_payment_method_enabled) {
        // Selected payment method is disabled so unsetting it
        unset($order->data['payment_method']);
        // Reapplying all discounts
        commerce_discount_commerce_cart_order_refresh($order_wrapper);
      }
    }
  }
}
