<?php

/**
 * @file
 * Default rules configuration for Payment Method Discounts.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_discount_payment_method_default_rules_configuration() {
  $rules = array();

  $rule = rules_reaction_rule();

  $rule->label = t('Commerce Discount Payment Method reaction on payment methods disabling');
  $rule->tags = array('Commerce Payment');
  $rule->active = TRUE;
  $rule->weight = 100;

  $rule
    ->event('commerce_payment_methods')
    ->action('commerce_discount_payment_method_disabling_reaction', array(
      'commerce_order:select' => 'commerce-order',
    ));

  $rules['commerce_discount_payment_method_disabling'] = $rule;

  return $rules;
}
